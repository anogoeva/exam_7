import React from 'react';
import "../../App.css";
import Coffee from '../../assets/Coffee.jpeg';
import Fries from '../../assets/Fries.jpg';
import Hamburger from '../../assets/Hamburger.jpg';
import HotDog from '../../assets/HotDog.jpg';
import Soda from '../../assets/Soda.jpeg';
import Tea from '../../assets/Tea.jpeg'


const Items = props => {
    let picture = null;

    if (props.name === "Coffee") {
        picture = Coffee;
    } else if (props.name === "Fries") {
        picture = Fries;
    } else if (props.name === "Hamburger") {
        picture = Hamburger;
    }  else if (props.name === "HotDog") {
        picture = HotDog;
    } else if (props.name === "Soda") {
        picture = Soda;
    } else picture = Tea;

    return (
        <div className="boxItem">
            <img src={picture} alt="#" width="70px" height="70px" onClick={props.onHeaderClick} className="boxItemImg"/>
            <p><strong  className="boxItemName">{props.name}</strong></p>
            <p><i className="boxItemCount">Price: {props.price}</i></p>
        </div>
    );
};

export default Items;
