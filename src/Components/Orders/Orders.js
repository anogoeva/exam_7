import React from 'react';


const Orders = props => {
    let divElements = [];

    if (props.count > 0) {
        divElements.push(<div>{props.name} - количество: <b>{props.count}</b> сумма: {props.addPrice} <i>KGS</i> <button onClick={props.onRemove}>x</button></div>)
    }
    return (
        <div className="ordersBox">
            <div className="orders">
                {divElements}
            </div>
        </div>
    );
};

export default Orders;