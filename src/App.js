import {useState} from "react";
import {nanoid} from "nanoid";
import './App.css'
import Items from "./Components/Items/Items";
import Orders from "./Components/Orders/Orders";

const App = () => {
    const [items, setItems] = useState([
        {name: 'Coffee', count: 0, price: 70, id: nanoid(), addPrice: 0},
        {name: 'Fries', count: 0, price: 45, id: nanoid(), addPrice: 0},
        {name: 'Hamburger', count: 0, price: 80, id: nanoid(), addPrice: 0},
        {name: 'HotDog', count: 0, price: 45, id: nanoid(), addPrice: 0},
        {name: 'Soda', count: 0, price: 40, id: nanoid(), addPrice: 0},
        {name: 'Tea', count: 0, price: 50, id: nanoid(), addPrice: 0},
    ]);
    const [totalPrice, setTotalPrice] = useState(0);
    const [totalCount, setTotalCount] = useState(0);
    const itemsComponents = items.map(item => (
        <Items
            key={item.id}
            id={item.id}
            name={item.name}
            count={item.count}
            price={item.price}
            onHeaderClick={() => addItem(item.id)}
        />
    ));
    const showItem = items.map(item => (
        <Orders
            key={item.id}
            name={item.name}
            count={item.count}
            addPrice={item.addPrice}
            id={item.id}
            onRemove={() => deleteItem(item.id)}
        />
    ));

    const addItem = id => {
        setItems(items.map(item => {
            if (item.id === id) {
                setTotalPrice(totalPrice + item.price);
                setTotalCount(totalCount + item.count);
                return {...item, count: item.count + 1, addPrice: item.addPrice + item.price};
            }
            return item;
        }));
    };

    const deleteItem = id => {
        setItems(items.map(item => {
            if (item.id === id) {
                if (item.count > 0) {
                    setTotalPrice(totalPrice - item.price);
                    setTotalCount(totalCount - item.count);
                    return {...item, count: item.count - 1};
                } else if (item.count <= 0) {
                    setTotalPrice(0);
                    setTotalCount(0);
                    return {...item, count: 0};
                }
            }
            return item;
        }));
    };

    return (
        <div className="appContainer">
            <fieldset className="orders">
                <legend className="title">Order Details</legend>
                {showItem}
                <p>=========================</p>
                <p>Total Price: {totalPrice} KGS</p>
            </fieldset>
            <fieldset className="box">
                <legend className="title">Add Items</legend>
                {itemsComponents}
            </fieldset>
        </div>
    );
};

export default App;
